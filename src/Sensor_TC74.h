#include <16F877A.h>
#device *=16
#device adc=8

#FUSES NOWDT                 	//No Watch Dog Timer
#FUSES XT                    	//Crystal osc <= 4mhz
#FUSES NOPUT                 	//No Power Up Timer
#FUSES NOPROTECT             	//Code not protected from reading
#FUSES BROWNOUT              	//Reset when brownout detected
#FUSES NOLVP                 	//No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                 	//No EE protection
#FUSES NOWRT                 	//Program memory not write protected
#FUSES NODEBUG               	//No Debug mode for ICD

#use delay(clock=4000000)

//Directiva que implementa por software el bus I2C
//#use i2c(Master,sda=PIN_C4,scl=PIN_C3)

//Directiva que implementa por hardware el bus I2C
#use i2c(master,sda=PIN_C4,scl=PIN_C3,slow=100000,FORCE_HW)
