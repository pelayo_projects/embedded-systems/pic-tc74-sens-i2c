///////////////////////////////////////////////////////////////////
//                    	    Pleguina                             //
//                         ~ TAREA 4 ~                           //
//                                                               //
// Programa que se comunica con un sensor de temperatura TC74    //
// presente en la placa de pr�cticas de Microchip PICDEM2 plus   //
// mediante bus I2C                                              //
//                                                               //
// La temperatura medida se muestra en la l�nea superior del LCD //
// presente en la placa. En la l�nea inferior se muestran si la  //
// temperatura ha subido, bajado o permanece constante respecto  //
// a la temperatura anterior.                                    //
//                                                               //
///////////////////////////////////////////////////////////////////

#include "Sensor_TC74.h"

//Se incluyen las funciones de manejo del LCD de caracteres presente 
//en la placa de pr�cticas PICDEM2 plus
#include "LCD_PICDEM2_plus_v2002.c"

//Definici�n de los caracteres de las flechas hacia arriba, hacia abajo y del s�mbolo de igualdad
#define flecha_arriba 0b00011000    //?
#define flecha_abajo 0b00011001     //?
#define igualdad 0b00111101         //=

//Variables para almacenar temperatura medida y anterior
//que se inicializa con un valor extremo
signed int   Temp_ant=0b01111111, Temperatura;

//Contador para el n�mero de medidas
int cont;
int i=0;

//El bit de reconocimiento (ACK) devuelto por el TC74 se guarda en
//la variable global Reconocimiento. 
//Si est� a cero es que hubo respuesta, SDA baj� a cero y si est� a 1 es que no hubo
//respuesta
boolean   Reconocimiento;

///////////////////////////////////////////////////////////////////////
char leer_temperatura(void)
//Funci�n que se comunica con el sensor de temperatura
//y recoge un byte que representa la temperatura en grados Cent�grados
//Si la temperatura es negativa, se recoge en complemento a 2
//Los valores posibles van desde -65� hasta 127�
{
   char lectura;

      i2c_start();             //Bit de start
   
   //Enviamos Direcci�n del sensor: 1001101 y bit de R/W = 0, recogemos bit de ACK
   //en Reconocimiento
      Reconocimiento=i2c_write(0b10011010);      
   
   if (!Reconocimiento)   //Si hubo respuesta, seguimos, si no no enviamos el resto
   {
         i2c_write(0x00);         //Comando: lectura registro de temperatura
         i2c_start();            //Repetici�n de start
         i2c_write(0b10011011);      //Direcci�n del sensor y bit de R/W = 1
         lectura=i2c_read(0);      //Recogemos temperatura y colocamos NO ACK
   }
   i2c_stop();         //En cualquier caso, cerramos la actividad del I2C
   return(lectura);   //Devolvemos el valor le�do, si no hubo respuesta, no tiene sentido
}

void enviar_temperatura(Temperatura)
{
   char valor_temperatura;
   for(i,i<cont,i++)
   {
      valor_temperatura=read_eeprom(i);
      delay_ms(500);  //ESperamos 0.5s
      printf("\n\rT%d = %cC\n\r",i,valor_temperatura,248);
   }
}

///////////////////////////////////////////////////////////////////////
void mostrar_temperatura(char Temperatura)
//Funci�n que recoge un byte que representa temperatura
//con valores negativos en complemento a 2 y env�a al LCD
//el signo + � - y los d�gitos correspondientes a la temperatura
{
   printf(lcd_putc,"T=");
   printf(lcd_putc,"%3d",Temperatura);
   lcd_putc(0);     //C�digo del s�mbolo de �
   lcd_putc('C');   //Enviamos la C
}

#int_TIMER1
void TIMER1_isr()
{
    int caracter;
    set_timer1(32768);     //Valor calculado para 4s a la frecuencia indicada
    Temperatura=leer_temperatura();
    mostrar_temperatura=Temperatura;
    write_eeprom(cont,Temperatura);
    printf("+");        //Indicador de que se est�n realizando medidas
    cont++;
    
    if(kbhit())  caracter=getch();   //Si hay un caracter disponible lo recojo

    if((caracter=='V') || (cont==8))    //Si el car�cter recibido es 'V' o el contador ha llegado a 8
    {
        cont=0;
        write_eeprom(8,cont);
    }
    else        return;
}

#int_RDA
RDA_isr()
{
   int   caract;

   if(kbhit())  caract=getch();   //Si hay un caracter disponible lo recojo
   else         return;              //y si no salgo

   if((caract=='T') || (cont==8))   //Si el car�cter recibido es 'T'
   {
       printf("\n\rT = ",caract); //Lo imprimo
       enviar_temperatura();
       cont=0;
       write_eeprom(0x2100,cont);
       set_timer1(32768);
   }
}
///////////////////////////////////////////////////////////////////////
void main()
{
      //Configuraciones iniciales
      setup_adc_ports(NO_ANALOGS);
      setup_adc(ADC_OFF);
      setup_psp(PSP_DISABLED);
      setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
      setup_timer_2(T2_DISABLED,0,1);
      setup_timer_1(T1_EXTERNAL|T1_DIV_BY_1);
      set_timer1(32768);
    
      cont=read_eeprom(8);
      if ((cont<0) || (cont>8))
      {
          cont=0;
          write_eeprom(8,cont);
      }

      lcd_reset();   //Reset software del LCD de caracteres

      lcd_init();      //Inicializaci�n del LCD de caracteres

      //Definici�n del caracter del s�mbolo de grados � en las posiciones 0x00 a 0x07 de la CGRAM
      //El c�digo de car�cter asignado al s�mbolo ser�a el 0
      lcd_set_cgram(0x00); //Situamos el puntero en la direcci�n 0 de la CGRAM
      //Y ahora vamos cargando en esa posici�n y en las siguientes el patr�n de representaci�n
      lcd_send_byte(1,0b00000110);  //Fila superior del car�cter �, direcci�n 0x00 de la CGRAM
      lcd_send_byte(1,0b00001001);  //segunda fila en la direcci�n 0x01
      lcd_send_byte(1,0b00001001);  //tercera fila en la direcci�n 0x02
      lcd_send_byte(1,0b00000110);
      lcd_send_byte(1,0b00000000);
      lcd_send_byte(1,0b00000000);
      lcd_send_byte(1,0b00000000);
      lcd_send_byte(1,0b00000000);  //octava fila en la direcci�n 0x07

      delay_ms(500); //Esperamos 0,5 segundos antes de entrar en el bucle continuo 

      enable_interrupts(INT_RDA);   //Activamos interrupciones por recepci�n
      enable_interrupts(INT_TIMER1);
      enable_interrupts(global);    //y la m�scara global
   
   while(1)   //Bucle de ejecuci�n continuo
   {
      Temperatura = leer_temperatura();   //Recogemos del sensor el valor de T
      if (Reconocimiento)   //Si no hubo respuesta Reconocimiento=1
      {               //mostramos mensaje en el LCD
         lcd_gotoxy(1,1);         
         printf(lcd_putc,"Sin resp. sensor");   
      }
      else   //Si hubo respuesta del sensor
      {
         lcd_gotoxy(5,1);      //Nos situamos en la l�nea superior
         printf(lcd_putc,"T=");   //y mostramos la temperatura
         mostrar_temperatura(Temperatura);
   
         if (Temperatura > Temp_ant)   //Comparamos valor medido con el anterior
         {                             //Si el valor medido supera al anterior, la temperatura sube
            Temperatura = Temp_ant;
            lcd_gotoxy(3,2);      //mostramos flecha hacia arriba 
            printf(lcd_putc,"Subiendo %c",flecha_arriba);
         }

         if (Temperatura < Temp_ant)   //Comparamos valor medido con el anterior
         {                             //Si el valor medido es inferior al anterior, la temperatura baja
            Temperatura = Temp_ant;
            lcd_gotoxy(3,2);      //mostramos flecha hacia abajo
            printf(lcd_putc,"Bajando %c",flecha_abajo);
         }
         if (Temperatura==Temp_ant)     //La temperatura permanece estable
         {
             Temperatura = Temp_ant;
             lcd_gotoxy(3,2);
             printf(lcd_putc,"Estable %c",igualdad);
         }
      }//cierre del else
   }//cierre del while(1)
}//cierre del main()
