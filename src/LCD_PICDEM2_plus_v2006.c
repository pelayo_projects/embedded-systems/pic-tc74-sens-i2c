////////////////////////////////////////////////////////////////////////////
////           Fichero LCD_PICDEM2_plus_v2006.C                         ////
////           Funciones para manejo de LCD con driver                  //// 
////                                                                    ////
////           Para PICDEM 2 PLUS versi�n 2006                          ////
////
////           Esta placa puede tener montado uno de estos dos tipos de LCDs:
////              * Modelo LUMEX LCD_SO1602 -> lee nibbles en orden inverso al habitual: parte baja primero                    ////
////              * Modelo OM16214 -> lee nibbles en orden habitual: parte alta primero 
////***************************************************************************************
/////////////////////////////////////////////////////////////////////////////////////////////
////          OJO: LUMEX LCD_SO1602 LEE los nibbles en orden inverso al habitual a los dem�s LCD: 
////          primero parte baja y luego la parte alta.
////          La escritura la hace en el orden habitual: 
////          primero se escribe el nibble de la parte alta y luego parte baja
////        
////          Para distinguir uno de otro, definimos una etiqueta para la compilaci�n condicional

//#define  LCD_OM16214 //Si se dispone de una placa con modelo que no sea OM16214, se debe comentar esta l�nea
                       //Si el modelo es OM16214 debe aparecer la l�nea sin comentar
//// ****************************************************************************************
///////////////////////////////////////////////////////////////////////////

// En la PICDEM 2 PLUS v2006 la conexi�n del LCD se realiza con pines del PORTD:
//
//     RD7  controla la alimentaci�n del LCD (+Vcc si la l�nea est� a 1)
//     RD6  es E (enable)
//     RD5  es RW
//     RD4  es RS
//     RD3  es DB7
//     RD2  es DB6
//     RD1  es DB5
//     RD0  es DB4
//   
#device PIC16F877A

struct lcd_pin_map {  
         int       
            data : 4;         // las 4 l�neas bajas ser�n las de datos (menos peso)
            BOOLEAN (rs);       //RD4 ser� rs      
            BOOLEAN rw;       //RD5 ser� rw          
            BOOLEAN enable;   //RD6 ser� enable             
            BOOLEAN vcc;      //RD7 controla vcc (mas peso al final)     
        } lcd;

// Asigno el lcd al puerto D. La directiva getenv devuelve la direccion
// de un registro especifico
// OJO, con la versi�n 4.120 del compilador no funciona la directiva getenv
// #locate lcd = getenv("sfr:PORTD") <----no servir�a
// Hacemos la asignaci�n de la direcci�n de manera directa
#locate lcd = 0x08      //Direcci�n del puerto D: PORTD en direcci�n 0x08
#byte tris_lcd = 0x88   //Para definir la direcci�n de los datos en el PORTD

#define lcd_type 2           // 0=5x7, 1=5x10, 2=2 lines
#define lcd_line_two 0x40    // LCD RAM address for the second line

/////////////////////////////////////////////////////////////////////////////////////////////
// La constante LCD_INIT_STRING[4] recoge los 4 c�digos de inicializaci�n que se envian
// al LCD y son muy importantes. Se deben cambiar para adaptar a otras necesidades.
/////////////////////////////////////////////////////////////////////////////////////////////

BYTE const LCD_INIT_STRING[4] = {0x28, 0x0C, 0x01, 0x06};
//
// El significado de los c�digos es el siguiente:
// 0x28 = 0010 1000 = 001 DL  N F **
// entonces DL=0 control a 4 bits (DL=0 ser�a control con 8 bits)
// N =1 significa 2 lineas  (N=0 ser�a 1 l�nea)
// F=0 significa, caracteres 5x8  (F=1 ser�a 5x10)
//
// 0x0C = 0000 1100 = 0000 1 D C B
// es el control de display ON/OFF
// D=1 significa diaplay ON
// C=0 significa cursor OFF (puede interesar poner C=1 para ver donde esta el cursor)
// B=0 sigifica parpadeo del cursor OFF
//
// 0x01 = 0000 0001 significa borrar display
//
// 0x06 = 0000 0110 = 0000 01 I/D S
// selecciona el modo de funcionamiento (Entry mode set)
// I/D = 1 significa incremento automatico del cursor
// S = 0 sifnifica sin desplazamiento del display
//

//Prototipos de las funciones posteriores
void lcd_init();
void lcd_reset();
byte lcd_read_byte();
void lcd_send_nibble(byte n);
void lcd_send_byte(byte address, byte n);
void lcd_gotoxy(byte x, byte y);
void lcd_putc(char c);
char lcd_getc(byte x, byte y);
void lcd_clr_line(char fila);
void lcd_set_cgram(char cgram_p);

/////////////////////////////////////////////////////////////////////////////////////////////
// Lee el byte se�alado por el puntero, 1� parte baja, 2� parte alta
// Si al llamar a esta funci�n rs=0, devuelve busy flag (BF) (+signif.) y direcci�n actual
/////////////////////////////////////////////////////////////////////////////////////////////
// OJO: Este LCD recoge los nibbles en orden inverso a los dem�s LCD: primero parte baja y 
// luego la parte alta
/////////////////////////////////////////////////////////////////////////////////////////////
BYTE lcd_read_byte() {
      BYTE low,high;
      tris_lcd = 0b00001111;    //Se definen las l�neas bajas como entradas
      lcd.rw = 1;
      delay_cycles(1);
      lcd.enable = 1;
      delay_cycles(1);
      #IFDEF LCD_OM16214
        high = lcd.data;  //con modelo LCD_OM16214 se lee primero la parte alta (lo habitual)
      #ELSE
        low = lcd.data; // Con LCD_SO1602 se lee primero la parte baja (ES AL REV�S QUE EN OTROS LCDs)
      #ENDIF
      
      lcd.enable = 0;
      delay_cycles(1);
      lcd.enable = 1;
      delay_us(1);
      
      #IFDEF LCD_OM16214
        low = lcd.data;   //con modelo OM16214 se lee despu�s la parte baja (m�s habitual)
      #ELSE
        high = lcd.data; // Con LCD_SO1602 se lee despues la parte alta (AL REV�S QUE EN OTROS LCDs)
      #ENDIF

      lcd.enable = 0;
      tris_lcd = 0b00000000;    //Volvemos a dejar el PORTD como puerto de salida
      return( (high<<4) | low);
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Env�a medio byte, los 4 bits m�s bajos de n
// Necesario poner rs y rw de modo adecuado y entrar con enable=0
/////////////////////////////////////////////////////////////////////////////////////////////

void lcd_send_nibble( BYTE n ) {
      lcd.data = n;
      delay_cycles(1);
      lcd.enable = 1;
      delay_us(2);
      lcd.enable = 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Env�a un byte (n) al registro de instrucciones (si address=0) o reg. de datos (address=1)
// Utiliza lcd_send_nibble(n) enviando primero nibble alto del byte
/////////////////////////////////////////////////////////////////////////////////////////////

void lcd_send_byte( BYTE address, BYTE n ) {

      lcd.rs = 0;
      while ( bit_test(lcd_read_byte(),7) ) ;
     //
     //delay_us(100);
      lcd.rs = address;
      delay_cycles(1);
      lcd.rw = 0;
      delay_cycles(1);
      lcd.enable = 0;
      lcd_send_nibble(n >> 4);
      lcd_send_nibble(n & 0xf);
 
}

///////////////////////////////////////////////////////////////////////////////////////////
/// Funci�n que inicializa el LCD, se deber�an cambiar bits para cambiar configuracion
///////////////////////////////////////////////////////////////////////////////////////////
void lcd_init() {
    BYTE i;
    tris_lcd = 0b00000000;  //PORTD de salida
    lcd.rs = 0;
    lcd.rw = 0;
    lcd.enable = 0;
    lcd.vcc = 1;
   
   //Env�os para resetear por software el LCD
    delay_ms(15);
    for(i=1;i<=3;++i) {
       lcd_send_nibble(3);
       delay_ms(5);
    }
    lcd_send_nibble(2);
    /////fin del reseteo por software
    
    //Se env�an ahora los comandos de inicializaci�n
    for(i=0;i<=3;++i){
       lcd_send_byte(0,LCD_INIT_STRING[i]);
      delay_ms(5);
   }
}

///////////////////////////////////////////////////////////////////////////////////////////
/// Funci�n que resetea el LCD por software
///////////////////////////////////////////////////////////////////////////////////////////
void lcd_reset()
{
    BYTE i;
    tris_lcd = 0b00000000;
    lcd.rs = 0;
    lcd.rw = 0;
    lcd.enable = 0;
    lcd.vcc = 1;
   
   //Env�os para resetear por software el LCD
    delay_ms(15);
    for(i=1;i<=3;++i) {
       lcd_send_nibble(0x03);
       delay_ms(5);
    }
    lcd_send_nibble(0x02);
    /////fin del reseteo por software


}
/////////////////////////////////////////////////////////////////////////////////////////////
// Sit�a el contador de direcciones en la DDRAM (para lectura o escritura posterior)
// x puede ir de 1 a 40, posici�n dentro de una l�nea (16 visibles)
// y puede ser 1 (l�nea 1) o 2 (l�nea 2)
/////////////////////////////////////////////////////////////////////////////////////////////

void lcd_gotoxy( BYTE x, BYTE y) {
   BYTE address;

   if(y!=1)
     address=lcd_line_two;
   else
     address=0;
   address+=x-1;
   lcd_send_byte(0,0b10000000|address);
}

////////////////////////////////////////////////////////////////////////////////////////////
// Env�a un car�cter c a la DDRAM del LCD, tambi�n algunos caracteres de control
////////////////////////////////////////////////////////////////////////////////////////////

void lcd_putc( char c) 
{
   switch (c) {
     case '\f'   : lcd_send_byte(0,1);    //Limpia la pantalla
                   delay_ms(2);
                   break;
     case '\n'   : lcd_gotoxy(1,2);       //Coloca puntero en 1� posicion de la 2� l�nea
                   break;
     case '\b'   : lcd_send_byte(0,0x10); //Retrocede una posici�n el cursor
                   break;
     case '\t'   : lcd_send_byte(0,0x14); //Avanza una posici�n el cursor
                   break;
     case '\r'   : lcd_send_byte(0,0x18); //Retrocede una posici�n la pantalla visible
                   break;
     case '\v'   : lcd_send_byte(0,0x1C); //Avanza una posici�n la pantalla visible
                   break;
     default     : lcd_send_byte(1,c);    //Env�a caracter a DDRAM,
                   break;                 //Si es una tira, los env�a todos uno a uno
             }
}

///////////////////////////////////////////////////////////////////////////////////////////
// Devuelve el car�cter situado en la posici�n x,y de la DDRAM
///////////////////////////////////////////////////////////////////////////////////////////

char lcd_getc( BYTE x, BYTE y) {
   char value;

    lcd_gotoxy(x,y);
    while ( bit_test(lcd_read_byte(),7) ); // espera a que el flag de ocupado est� a 0
    lcd.rs=1;
    value = lcd_read_byte();
    lcd.rs=0;
    return(value);
}

////////////////////////////////////////////////////////////////////////////////////
// Limpia la linea correspondiente y se situa al principio de la misma            //
////////////////////////////////////////////////////////////////////////////////////
void lcd_clr_line(char fila)
{
   int j;

   lcd_gotoxy(1,fila);
    for (j=0;j<40;j++) lcd_putc(' ');

    lcd_gotoxy(1,fila);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Funci�n que coloca el puntero en una direcci�n de la CGRAM para definir luego nuevos caracteres       //
///////////////////////////////////////////////////////////////////////////////////////////////////////////
void lcd_set_cgram(char cgram_p)
{
   lcd_send_byte(0,0b01000000|cgram_p);   //Las direcciones de CGRAM empiezan por 01xxxxxx
}
